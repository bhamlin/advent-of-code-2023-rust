mod scorecard;

use aoc_tools::{advent_init, error::Nothing, AdventInputSource, AdventProblemPart, ProblemInput};
use log::*;

use crate::scorecard::build_cards_from_input;

const DAY: u8 = 4;
const SAMPLE_ONE: i128 = 13;
const SAMPLE_TWO: i128 = 30;
const RESULT_ONE: i128 = 23_441;
const RESULT_TWO: i128 = 5_923_918;

fn main() -> Nothing {
    let input = advent_init(DAY)?;

    match &input.part {
        AdventProblemPart::One => part_one(&input),
        AdventProblemPart::Two => part_two(&input),
    }

    Ok(())
}

fn part_one(input: &ProblemInput) {
    let cards = build_cards_from_input(&input.lines);

    let result: u64 = cards
        .iter()
        .map(|e| e.matched_numbers.len())
        .map(|len| calculate_points(len) as u64)
        .sum();

    info!("Day {DAY:<2} - Part 1: {result}");
    match &input.source {
        AdventInputSource::Sample(_) => info!("         Sample: {SAMPLE_ONE}"),
        AdventInputSource::Input => info!("         Result: {RESULT_ONE}"),
    }
}

fn calculate_points(len: usize) -> u32 {
    match len {
        0 => 0,
        v => 2u32.pow((v as u32) - 1),
    }
}

fn part_two(input: &ProblemInput) {
    let cards = build_cards_from_input(&input.lines);
    let card_count = cards.len();

    let mut counts = vec![1_usize; card_count];
    cards.iter().for_each(|e| {
        let i = e.card_id as usize;
        let n = e.matched_numbers.len();
        let inc = *counts.get(i).unwrap();
        if n > 0 {
            (i + 1..=i + n).for_each(|index| {
                if let Some(count) = counts.get_mut(index) {
                    *count += inc;
                }
            })
        }
    });

    let result: usize = counts.iter().sum();
    info!("Day {DAY:<2} - Part 2: {result}");
    match &input.source {
        AdventInputSource::Sample(_) => info!("         Sample: {SAMPLE_TWO}"),
        AdventInputSource::Input => info!("         Result: {RESULT_TWO}"),
    }
}
