use itertools::Itertools;
use log::debug;

#[derive(Default)]
pub struct ScoreCard {
    pub card_id: u8,
    pub winning_numbers: Vec<u8>,
    pub playing_numbers: Vec<u8>,
    pub matched_numbers: Vec<u8>,
}

impl ScoreCard {
    #[allow(clippy::ptr_arg)]
    pub fn load_from_input((id, input): (usize, &String)) -> Option<Self> {
        let card_id = id as u8;
        let (_, numbers) = input.split_once(": ")?;
        let (wins, draw) = numbers.split_once(" | ")?;
        let winning_numbers = split_whitespace_and_parse(wins);
        let playing_numbers = split_whitespace_and_parse(draw);

        // Identify matched numbers
        let matched_numbers = playing_numbers
            .iter()
            .filter(|v| winning_numbers.contains(*v))
            .cloned()
            .collect();
        debug!("Card {card_id}: {matched_numbers:?}");

        Some(Self {
            card_id,
            winning_numbers,
            playing_numbers,
            matched_numbers,
        })
    }
}

pub fn build_cards_from_input(input: &[String]) -> Vec<ScoreCard> {
    input
        .iter()
        .enumerate()
        .filter_map(ScoreCard::load_from_input)
        .collect_vec()
}

fn split_whitespace_and_parse(input: &str) -> Vec<u8> {
    input
        .split_whitespace()
        .flat_map(str::parse::<u8>)
        .collect_vec()
}
