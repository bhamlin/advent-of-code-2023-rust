use aoc_tools::{
    advent_init,
    error::{AdventError, Nothing},
    AdventInputSource, AdventProblemPart, ProblemInput,
};
use log::*;
use std::{cmp, collections::HashMap};

const DAY: u8 = 2;
const SAMPLE_ONE: i128 = 8;
const SAMPLE_TWO: i128 = 2286;
const RESULT_ONE: i128 = 2810;
const RESULT_TWO: i128 = 69110;

type Hand = HashMap<Cube, u16>;

#[derive(Debug)]
struct Game {
    pub id: u16,
    pub hands: Vec<Hand>,
}

#[derive(Clone, Copy, Debug, Eq, Hash, PartialEq)]
enum Cube {
    Red,
    Green,
    Blue,
}

impl TryFrom<&str> for Cube {
    type Error = AdventError;

    fn try_from(value: &str) -> Result<Self, Self::Error> {
        match value {
            "red" => Ok(Self::Red),
            "green" => Ok(Self::Green),
            "blue" => Ok(Self::Blue),
            _ => Err(AdventError::ParsingError),
        }
    }
}

fn main() -> Nothing {
    let input = advent_init(DAY)?;

    match &input.part {
        AdventProblemPart::One => part_one(&input),
        AdventProblemPart::Two => part_two(&input),
    }

    Ok(())
}

fn display_hand(hand: &Hand) -> String {
    format!(
        "Red: {:2}, Green: {:2}, Blue: {:2}",
        hand.get(&Cube::Red).unwrap_or(&0),
        hand.get(&Cube::Green).unwrap_or(&0),
        hand.get(&Cube::Blue).unwrap_or(&0),
    )
}

fn parse_game(line: &str) -> Option<Game> {
    let (game_tag, hand_list) = line.split_once(": ")?;
    let (_, game_id) = game_tag.split_once(' ')?;
    let id = game_id.parse::<u16>().ok()?;
    // debug!(target:"parse_game", "[{}] / [{}]", game_tag, hand_list);
    let hands = parse_hand_list(hand_list)?;

    Some(Game { id, hands })
}

fn parse_hand_list(line: &str) -> Option<Vec<Hand>> {
    let hands = line.split("; ").collect::<Vec<_>>();
    let mut output = Vec::with_capacity(hands.len());
    // debug!(target:"parse_hand_list", "{hands:?}");
    for hand in hands {
        let result = parse_hand(hand)?;
        output.push(result);
    }

    Some(output)
}

fn parse_hand(hand: &str) -> Option<Hand> {
    let cube_list = hand.split(", ").collect::<Vec<_>>();
    let mut output = Hand::with_capacity(cube_list.len());
    // debug!(target:"parse_hand", "{cube_list:?}");
    for cube_entry in cube_list {
        if let Some((qty, cube)) = cube_entry.split_once(' ') {
            output.insert(Cube::try_from(cube).ok()?, qty.parse::<u16>().ok()?);
        }
    }

    Some(output)
}

fn get_cube_counts(game: &Game) -> Hand {
    let mut result = Hand::new();

    for hand in game.hands.iter() {
        for (&color, &count) in hand.iter() {
            result
                .entry(color)
                .and_modify(|value| *value = cmp::max(*value, count))
                .or_insert(count);
        }
    }

    result
}

fn test_cube_counts(max_hand: &Hand, limit: &Hand) -> bool {
    for (color, &count) in max_hand.iter() {
        let max_value = limit.get(color).copied().unwrap_or(0);
        if count > max_value {
            return false;
        }
    }

    true
}

fn get_cube_power(max_hand: &Hand) -> u32 {
    max_hand.values().fold(1, |a, &e| a * (e as u32))
}

fn part_one(input: &ProblemInput) {
    let cube_counts = Hand::from([
        // Given counts
        (Cube::Red, 12),
        (Cube::Green, 13),
        (Cube::Blue, 14),
    ]);

    debug!(target:"part_one", "----------------------------------");
    debug!(target:"part_one", "test: {}", display_hand(&cube_counts));
    debug!(target:"part_one", "----------------------------------");

    let result: u32 = input
        .lines
        .iter()
        .filter_map(|line| parse_game(line.as_str()))
        .map(|game| {
            // let test = check_cube_counts(&game, &cube_counts);
            let max = get_cube_counts(&game);
            let test = test_cube_counts(&max, &cube_counts);
            debug!(target:"part_one", " max: {} => ({test})", display_hand(&max));
            if test {
                game.id as u32
            } else {
                0
            }
        })
        .sum();

    info!("Day {DAY:<2} - Part 1: {result}");
    match &input.source {
        AdventInputSource::Sample(_) => info!("         Sample: {SAMPLE_ONE}"),
        AdventInputSource::Input => info!("         Result: {RESULT_ONE}"),
    }
}

fn part_two(input: &ProblemInput) {
    let result: u32 = input
        .lines
        .iter()
        .filter_map(|line| parse_game(line.as_str()))
        .map(|game| {
            let max = get_cube_counts(&game);
            let power = get_cube_power(&max);
            debug!(target:"part_two", " max: {} => ({power})", display_hand(&max));
            power
        })
        .sum();

    info!("Day {DAY:<2} - Part 2: {result}");
    match &input.source {
        AdventInputSource::Sample(_) => info!("         Sample: {SAMPLE_TWO}"),
        AdventInputSource::Input => info!("         Result: {RESULT_TWO}"),
    }
}
