use aoc_tools::{point::PointI32 as Point, pt, ProblemInput};
use std::collections::HashMap;

pub type SchematicData = HashMap<Point, char>;

pub struct Schematic {
    data: SchematicData,
}

impl Schematic {
    pub fn load_from_input(input: &ProblemInput) -> Self {
        Schematic {
            data: input
                .lines
                .iter()
                .enumerate()
                .flat_map(|(y, line)| {
                    line.chars()
                        .enumerate()
                        .filter_map(|(x, char)| {
                            if char != '.' {
                                Some((pt!(x as i32, y as i32), char))
                            } else {
                                None
                            }
                        })
                        .collect::<Vec<_>>()
                })
                .collect(),
        }
    }

    pub fn get_value_at(&self, loc: &Point) -> Option<char> {
        self.data.get(loc).cloned()
    }

    pub fn is_numeric_at(&self, loc: &Point) -> Option<bool> {
        Some(self.data.get(loc)?.is_numeric())
    }

    // pub fn find(&self, char: char) -> Vec<Point> {
    //     self.data
    //         .iter()
    //         .filter_map(|(k, &v)| if v == char { Some(k) } else { None })
    //         .cloned()
    //         .collect::<Vec<_>>()
    // }

    pub fn find_func(&self, filter_func: fn(&char) -> bool) -> Vec<Point> {
        self.data
            .iter()
            .filter(|(_, c)| filter_func(c))
            .map(|(p, _)| p)
            .cloned()
            .collect::<Vec<_>>()
    }
}
