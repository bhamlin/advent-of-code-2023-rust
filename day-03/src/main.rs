mod schema;

use crate::schema::Schematic;
use aoc_tools::{
    advent_init, error::Nothing, point::PointI32 as Point, pt, AdventInputSource,
    AdventProblemPart, ProblemInput,
};
use itertools::Itertools;
use log::*;
use std::collections::HashMap;

const DAY: u8 = 3;
const SAMPLE_ONE: i128 = 4_361;
const SAMPLE_TWO: i128 = 467_835;
const RESULT_ONE: i128 = 546_563;
const RESULT_TWO: i128 = 91_031_374;

fn main() -> Nothing {
    let input = advent_init(DAY)?;

    match &input.part {
        AdventProblemPart::One => part_one(&input),
        AdventProblemPart::Two => part_two(&input),
    }

    Ok(())
}

fn part_one(input: &ProblemInput) {
    let schematic = Schematic::load_from_input(input);
    let values = part_one_logic(schematic, |c| !c.is_numeric());

    debug!("Found {} unique values", values.len());

    let result: i32 = values.iter().sum();

    info!("Day {DAY:<2} - Part 1: {result}");
    match &input.source {
        AdventInputSource::Sample(_) => info!("         Sample: {SAMPLE_ONE}"),
        AdventInputSource::Input => info!("         Result: {RESULT_ONE}"),
    }
}

fn part_one_logic(schematic: Schematic, filter_func: fn(&char) -> bool) -> Vec<i32> {
    // let target_locs = schematic.find_func(filter_func);
    // let filtered_locs = target_locs.iter().flat_map(|loc| {
    //     loc.surrounding_points()
    //         .iter()
    //         .filter_map(|loc| match schematic.get_value_at(loc)?.is_numeric() {
    //             true => Some(*loc),
    //             false => None,
    //         })
    //         .collect_vec()
    // });
    // let found_numbers = filtered_locs
    //     .map(|loc| build_char_stream(&schematic, loc))
    //     .collect::<HashMap<_, _>>()
    //     .values()
    //     .cloned()
    //     .collect_vec();
    //
    // return found_numbers;

    schematic
        // Filter to locations with desired characters
        .find_func(filter_func)
        .iter()
        .flat_map(|loc| {
            // Check points around locations, return those with numbers
            loc.surrounding_points()
                .iter()
                .filter_map(|loc| {
                    let c = schematic.get_value_at(loc)?;
                    match c.is_numeric() {
                        true => Some(*loc),
                        false => None,
                    }
                })
                .collect_vec()
        })
        // Build numbers from characters
        .map(|loc| build_char_stream(&schematic, loc))
        // Build HashMap from tuples to filter out duplicates based on the location in the input
        .collect::<HashMap<_, _>>()
        .values()
        .cloned()
        .collect_vec()
}

fn part_two(input: &ProblemInput) {
    let schematic = Schematic::load_from_input(input);
    let values = part_two_logic(schematic, 2, |c| *c == '*');

    debug!("Found {} unique values", values.len());

    let result: i32 = values.iter().sum();

    info!("Day {DAY:<2} - Part 2: {result}");
    match &input.source {
        AdventInputSource::Sample(_) => info!("         Sample: {SAMPLE_TWO}"),
        AdventInputSource::Input => info!("         Result: {RESULT_TWO}"),
    }
}

fn part_two_logic(
    schematic: Schematic,
    count_req: usize,
    filter_func: fn(&char) -> bool,
) -> Vec<i32> {
    schematic
        // Filter to locations with desired characters
        .find_func(filter_func)
        .iter()
        .map(|loc| {
            // Check points around locations, return those with numbers
            loc.surrounding_points()
                .iter()
                .filter_map(|loc| match schematic.get_value_at(loc)?.is_numeric() {
                    true => Some(*loc),
                    false => None,
                })
                .collect_vec()
        })
        // I need a Vec of Vecs because I must filter based on quantity found per symbol
        .map(|set| {
            set.iter()
                // Build numbers from characters
                .map(|loc| build_char_stream(&schematic, *loc))
                .collect_vec()
        })
        // HashMap each list to remove duplicates
        .map(|list| {
            let mut map = HashMap::<Point, i32>::new();
            list.iter().for_each(|&(k, v)| {
                map.insert(k, v);
            });
            map
        })
        // Remove maps that don't have exactly two entries
        .filter(|map| map.len() == count_req)
        // Value calculation is provided as multiple found values together
        .map(|map| map.values().fold(1, |a, v| a * *v))
        .collect_vec()
}

fn build_char_stream(schematic: &Schematic, entry: Point) -> (Point, i32) {
    let go_left = pt!(-1, 0);
    let go_right = pt!(1, 0);
    let mut pos = entry;
    let mut output = String::new();

    // Go right
    while let Some(true) = schematic.is_numeric_at(&(pos + go_left)) {
        pos += go_left;
    }

    // Go left
    while let Some(true) = schematic.is_numeric_at(&(pos)) {
        output.push(schematic.get_value_at(&pos).unwrap());
        pos += go_right;
    }

    let value = output.parse().unwrap();
    trace!(target:"char_stream", "Found: {}@{}", value, pos);

    (pos, value)
}
