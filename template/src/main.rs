#![allow(dead_code, unused_imports, unused_mut, unused_variables)]

use aoc_tools::{advent_init, error::Nothing, AdventInputSource, AdventProblemPart, ProblemInput};
use log::*;

const DAY: u8 = 1;
const SAMPLE_ONE: i128 = 0;
const SAMPLE_TWO: i128 = 0;
const RESULT_ONE: i128 = 0;
const RESULT_TWO: i128 = 0;

fn main() -> Nothing {
    let input = advent_init(DAY)?;

    match &input.part {
        AdventProblemPart::One => part_one(&input),
        AdventProblemPart::Two => part_two(&input),
    }

    Ok(())
}

fn part_one(input: &ProblemInput) {
    let mut result = 0;
    info!("Day {DAY:<2} - Part 1: {result}");
    match &input.source {
        AdventInputSource::Sample(_) => info!("         Sample: {SAMPLE_ONE}"),
        AdventInputSource::Input => info!("         Result: {RESULT_ONE}"),
    }
}

fn part_two(input: &ProblemInput) {
    let mut result = 0;
    info!("Day {DAY:<2} - Part 2: {result}");
    match &input.source {
        AdventInputSource::Sample(_) => info!("         Sample: {SAMPLE_TWO}"),
        AdventInputSource::Input => info!("         Result: {RESULT_TWO}"),
    }
}
