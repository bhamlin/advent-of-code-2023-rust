use core::fmt;
use std::{
    fmt::Display,
    ops::{Add, AddAssign, Sub, SubAssign},
};

#[derive(Clone, Copy, Eq, Hash, PartialEq)]
pub struct Point<T>
where
    T: Add<Output = T> + AddAssign + Sub<Output = T> + SubAssign + Display,
{
    pub x: T,
    pub y: T,
}

impl<T> Add for Point<T>
where
    T: Add<Output = T> + AddAssign + Sub<Output = T> + SubAssign + Display,
{
    type Output = Point<T>;

    fn add(self, rhs: Self) -> Self::Output {
        Self {
            x: self.x.add(rhs.x),
            y: self.y.add(rhs.y),
        }
    }
}

impl<T> AddAssign for Point<T>
where
    T: Add<Output = T> + AddAssign + Sub<Output = T> + SubAssign + Display,
{
    fn add_assign(&mut self, rhs: Self) {
        self.x += rhs.x;
        self.y += rhs.y;
    }
}

impl<T> Sub for Point<T>
where
    T: Add<Output = T> + AddAssign + Sub<Output = T> + SubAssign + Display,
{
    type Output = Point<T>;

    fn sub(self, rhs: Self) -> Self::Output {
        Self {
            x: self.x.sub(rhs.x),
            y: self.y.sub(rhs.y),
        }
    }
}

impl<T> SubAssign for Point<T>
where
    T: Add<Output = T> + AddAssign + Sub<Output = T> + SubAssign + Display,
{
    fn sub_assign(&mut self, rhs: Self) {
        self.x -= rhs.x;
        self.y -= rhs.y;
    }
}

impl<T> From<(T, T)> for Point<T>
where
    T: Add<Output = T> + AddAssign + Sub<Output = T> + SubAssign + Display,
{
    fn from((x, y): (T, T)) -> Self {
        Point::from_xy(x, y)
    }
}

impl<T> fmt::Debug for Point<T>
where
    T: Add<Output = T> + AddAssign + Sub<Output = T> + SubAssign + Display,
{
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        std::fmt::Display::fmt(&self, f)
    }
}

impl<T> Display for Point<T>
where
    T: Add<Output = T> + AddAssign + Sub<Output = T> + SubAssign + Display,
{
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "pt!({}, {})", self.x, self.y)
    }
}

impl<T> Point<T>
where
    T: Add<Output = T> + AddAssign + Sub<Output = T> + SubAssign + Display,
{
    pub fn from_xy(x: T, y: T) -> Self {
        Self { x, y }
    }
}

pub type PointI32 = Point<i32>;
pub type PointI64 = Point<i64>;
pub type PointI128 = Point<i128>;

impl Point<i32> {
    pub fn surrounding_points(&self) -> Vec<Point<i32>> {
        [
            PointI32::from((-1, -1)),
            PointI32::from((-1, 0)),
            PointI32::from((-1, 1)),
            PointI32::from((0, -1)),
            PointI32::from((0, 1)),
            PointI32::from((1, -1)),
            PointI32::from((1, 0)),
            PointI32::from((1, 1)),
        ]
        .iter()
        .map(|shift| *self + *shift)
        .collect()
    }
}

impl Point<i64> {
    pub fn surrounding_points(&self) -> Vec<Point<i64>> {
        [
            PointI64::from((-1, -1)),
            PointI64::from((-1, 0)),
            PointI64::from((-1, 1)),
            PointI64::from((0, -1)),
            PointI64::from((0, 1)),
            PointI64::from((1, -1)),
            PointI64::from((1, 0)),
            PointI64::from((1, 1)),
        ]
        .iter()
        .map(|shift| *self + *shift)
        .collect()
    }
}

impl Point<i128> {
    pub fn surrounding_points(&self) -> Vec<Point<i128>> {
        [
            PointI128::from((-1, -1)),
            PointI128::from((-1, 0)),
            PointI128::from((-1, 1)),
            PointI128::from((0, -1)),
            PointI128::from((0, 1)),
            PointI128::from((1, -1)),
            PointI128::from((1, 0)),
            PointI128::from((1, 1)),
        ]
        .iter()
        .map(|shift| *self + *shift)
        .collect()
    }
}

#[macro_export]
macro_rules! pt {
    ($x:expr, $y:expr) => {
        aoc_tools::point::Point::from(($x, $y))
    };
}
