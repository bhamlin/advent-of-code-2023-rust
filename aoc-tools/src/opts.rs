use clap::{Parser, Subcommand};

#[derive(Debug, Parser)]
pub struct AdventOptions {
    #[clap(subcommand)]
    pub source: SourceSubcommand,
}

#[derive(Debug, Subcommand)]
pub enum SourceSubcommand {
    Sample(SourcePart),
    Part(SourcePart),
}

#[derive(Debug, Parser)]
pub struct SourcePart {
    pub part: u8,
    pub path: Option<String>,
}

pub fn parse_args() -> AdventOptions {
    AdventOptions::parse()
}
