use std::fmt::Display;

use error_stack::Context;

pub type Nothing = error_stack::Result<(), AdventError>;
pub type AdventResult<T> = error_stack::Result<T, AdventError>;

#[derive(Debug)]
pub enum AdventError {
    ReadFailure,
    ParsingError,
    UnableToFindFile,
    UnableToCreateFile,
    UnsupportedPartValue,
}

impl Context for AdventError {}
impl Display for AdventError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let message = match self {
            Self::ReadFailure => "Failure reading from file",
            Self::ParsingError => "Parsing error",
            Self::UnableToFindFile => "File not found",
            Self::UnableToCreateFile => "Unable to create file",
            Self::UnsupportedPartValue => "Unsupported problem part value",
        };

        write!(f, "{message}")
    }
}
