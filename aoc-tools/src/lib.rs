use env_logger::{Builder, Env};
use error::{AdventError, AdventResult};
use error_stack::{Report, ResultExt};
use log::debug;
use opts::{AdventOptions, SourceSubcommand};
use std::{
    fs,
    io::{BufRead, BufReader},
};

pub mod error;
pub mod opts;
pub mod point;

pub enum AdventInputSource {
    Sample(u8),
    Input,
}

pub enum AdventProblemPart {
    One,
    Two,
}

impl TryFrom<u8> for AdventProblemPart {
    type Error = Report<AdventError>;

    fn try_from(value: u8) -> Result<Self, Self::Error> {
        match value {
            1 => Ok(AdventProblemPart::One),
            2 => Ok(AdventProblemPart::Two),
            n => Err(Report::new(AdventError::UnsupportedPartValue)
                .attach_printable(format!("Unsupported part value of {n}"))),
        }
    }
}

pub struct ProblemInput {
    pub lines: Vec<String>,
    pub part: AdventProblemPart,
    pub opts: AdventOptions,
    pub source: AdventInputSource,
}

pub fn advent_init(day: u8) -> AdventResult<ProblemInput> {
    Builder::from_env(Env::default().default_filter_or(log::Level::Info.as_str())).init();
    let opts = opts::parse_args();

    let (lines, part, source) = match &opts.source {
        SourceSubcommand::Sample(source_part) => (
            load_input(if let Some(path) = &source_part.path {
                path.clone()
            } else {
                build_input_source_path(day, &AdventInputSource::Sample(source_part.part))
            })?,
            AdventProblemPart::try_from(source_part.part)?,
            AdventInputSource::Sample(source_part.part),
        ),
        SourceSubcommand::Part(source_part) => (
            load_input(if let Some(path) = &source_part.path {
                path.clone()
            } else {
                build_input_source_path(day, &AdventInputSource::Input)
            })?,
            AdventProblemPart::try_from(source_part.part)?,
            AdventInputSource::Input,
        ),
    };

    Ok(ProblemInput {
        lines,
        part,
        opts,
        source,
    })
}

fn build_input_source_path(day: u8, source: &AdventInputSource) -> String {
    match source {
        AdventInputSource::Sample(part) => format!("data/samples/day-{day:02}-part{part}.txt"),
        AdventInputSource::Input => format!("data/inputs/day-{day:02}.txt"),
    }
}

// fn load_input_by_source(day: u8, source: &AdventInputSource) -> AdventResult<Vec<String>> {
fn load_input(problem_path: String) -> AdventResult<Vec<String>> {
    // Should really make sure that day is [1, 25]ish
    // let problem_path = build_input_source_path(day, source);

    debug!("Reading input from path: '{problem_path}'");

    let problem_file = open_or_create(&problem_path)?;
    let problem_file_reader = BufReader::new(problem_file);
    let mut lines = Vec::new();

    for line in problem_file_reader.lines().map_while(Result::ok) {
        // let line = line.change_context_lazy(|| error::AdventError::ReadFailure)?;
        lines.push(line);
    }

    Ok(lines)
}

fn open_or_create(path: &str) -> AdventResult<fs::File> {
    let file = fs::File::open(path)
        .change_context_lazy(|| error::AdventError::UnableToFindFile)
        .attach_printable_lazy(|| format!("Unable to open '{}'", path));
    if file.is_err() {
        fs::OpenOptions::new()
            .create(true)
            .write(true)
            .open(path)
            .change_context_lazy(|| error::AdventError::UnableToCreateFile)
            .attach_printable_lazy(|| format!("Unable to create '{}'", path))?;
    }

    file
}
