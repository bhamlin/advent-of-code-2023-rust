#![allow(dead_code, unused_mut, unused_variables)]

mod data;
mod parser;

use aoc_tools::{advent_init, error::Nothing, AdventInputSource, AdventProblemPart, ProblemInput};
use data::StepMap;
use itertools::Itertools;
use lcmx::lcmx;
use log::*;
use std::collections::HashMap;

use crate::parser::load_map;

const DAY: u8 = 8;
const SAMPLE_ONE: i128 = 6;
const SAMPLE_TWO: i128 = 30;
const RESULT_ONE: i128 = 20_221;
const RESULT_TWO: i128 = 14_616_363_770_447;

fn main() -> Nothing {
    let input = advent_init(DAY)?;

    match &input.part {
        AdventProblemPart::One => part_one(&input),
        AdventProblemPart::Two => part_two(&input),
    }

    Ok(())
}

fn part_one(input: &ProblemInput) {
    let map = load_map(&input.lines);
    let loop_lengths = get_loop_lengths(&map, part_one_starts, |state| state.eq("ZZZ"));
    let result: usize = loop_lengths.values().sum();

    info!("Day {DAY:<2} - Part 1: {result}");
    match &input.source {
        AdventInputSource::Sample(_) => info!("         Sample: {SAMPLE_ONE}"),
        AdventInputSource::Input => info!("         Result: {RESULT_ONE}"),
    }
}

#[inline]
fn part_one_starts(keys: Vec<&str>) -> Vec<&str> {
    keys.iter().filter(|&&e| e.eq("AAA")).cloned().collect_vec()
}

fn part_two(input: &ProblemInput) {
    let map = load_map(&input.lines);
    let loop_lengths = get_loop_lengths(&map, part_two_starts, |state| state.ends_with('Z'));
    let values = loop_lengths.values().cloned().collect_vec();
    let result = lcmx(&values).unwrap();

    info!("Day {DAY:<2} - Part 2: {result}");
    match &input.source {
        AdventInputSource::Sample(_) => info!("         Sample: {SAMPLE_TWO}"),
        AdventInputSource::Input => info!("         Result: {RESULT_TWO}"),
    }
}

#[inline]
fn part_two_starts(keys: Vec<&str>) -> Vec<&str> {
    keys.iter()
        .filter(|&&e| e.ends_with('A'))
        .cloned()
        .collect_vec()
}

fn get_loop_lengths(
    map: &StepMap,
    starts: fn(Vec<&str>) -> Vec<&str>,
    is_a_goal: fn(&str) -> bool,
) -> HashMap<&str, usize> {
    let keys = map.step_map.keys().map(|e| e.as_str()).collect_vec();
    let start_keys = starts(keys);
    let mut lengths = HashMap::with_capacity(start_keys.len());
    debug!(target:"get_loop_lengths", "Starts: {start_keys:?}");

    for start_key in start_keys {
        if let Some(len) = get_loop_len(map, start_key, &is_a_goal) {
            lengths.insert(start_key, len);
        }
    }

    lengths
}

fn get_loop_len(map: &StepMap, start_key: &str, is_a_goal: &fn(&str) -> bool) -> Option<usize> {
    let pattern = &map.step_pattern[..];
    let p_len = pattern.len();

    trace!(target:"get_loop_len", "{pattern:?} {p_len}");
    let mut index = 0;
    let mut key = start_key;

    while !is_a_goal(key) {
        let side = pattern.get(index % p_len)?;
        let step = map.step_map.get(key)?;
        key = &step[side.to_usize()];
        trace!(target:"get_loop_len",
            "[{:6}] ({}) {} -> {:?} -> {}",
            index,
            side.to_usize(),
            side,
            step,
            key
        );

        index += 1;
    }

    Some(index)
}
