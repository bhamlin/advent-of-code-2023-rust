use std::{collections::HashMap, fmt::Display};

pub type Step = [String; 2];

#[derive(Debug, PartialEq, Eq)]
pub enum Side {
    Left,
    Right,
}

impl Display for Side {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let display = match self {
            Self::Left => "Left",
            Self::Right => "Right",
        };

        write!(f, "{}", display)
    }
}

impl Side {
    pub fn from_char(value: char) -> Option<Self> {
        match value {
            'L' => Some(Self::Left),
            'R' => Some(Self::Right),
            _ => None,
        }
    }

    pub fn to_usize(&self) -> usize {
        match self {
            Self::Left => 0,
            Self::Right => 1,
        }
    }
}

pub struct StepMap {
    pub step_pattern: Vec<Side>,
    pub step_map: HashMap<String, Step>,
}
