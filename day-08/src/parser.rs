use crate::data::{Side, Step, StepMap};
use itertools::Itertools;
use log::trace;
use std::collections::HashMap;

pub fn load_map(lines: &[String]) -> StepMap {
    // First line should be a LR pattern
    let step_pattern = lines
        .first()
        .unwrap()
        .chars()
        .filter_map(Side::from_char)
        .collect_vec();
    trace!(target:"load_map", "{step_pattern:?}");

    // Lines 2 onward should be a map nodes with a pair of destination nodes
    // Really should verify that the node list we import is viable
    let step_map = lines
        .iter()
        .skip(2)
        .filter(|line| line.contains('='))
        .filter_map(split_line)
        .collect::<HashMap<_, _>>();
    trace!(target:"load_map", "{step_map:?}");

    StepMap {
        step_pattern,
        step_map,
    }
}

#[allow(clippy::ptr_arg)]
fn split_line(input: &String) -> Option<(String, Step)> {
    let (tag, steps) = input.split_once(" = ")?;
    let (leaf_l, leaf_r) = steps[1..9].split_once(", ")?;

    Some((tag.into(), [leaf_l.into(), leaf_r.into()]))
}
