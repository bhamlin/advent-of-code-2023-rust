use aoc_tools::{advent_init, error::Nothing, AdventInputSource, AdventProblemPart, ProblemInput};
use log::*;

const DAY: u8 = 1;
const SAMPLE_ONE: i128 = 142;
const SAMPLE_TWO: i128 = 281;
const RESULT_ONE: i128 = 54331;
const RESULT_TWO: i128 = 54518;

fn main() -> Nothing {
    let input = advent_init(DAY)?;

    match &input.part {
        AdventProblemPart::One => part_one(&input),
        AdventProblemPart::Two => part_two(&input),
    }

    Ok(())
}

fn nth_digit(input: &str, n: Option<usize>) -> Option<u32> {
    let limit = if let Some(v) = n { v } else { input.len() };
    let mut count = 0;
    let mut last_num = None;

    for c in input.chars() {
        if count == limit {
            break;
        }

        if c.is_numeric() {
            last_num = Some(c);
            count += 1;
        }
    }

    last_num.map(|c| c.to_digit(10).unwrap())
}

fn part_one(input: &ProblemInput) {
    let result: u32 = input
        .lines
        .iter()
        .filter_map(|line| first_and_last_digit(line.as_str()))
        .sum();

    info!("Day {DAY:<2} - Part 1: {result}");
    match &input.source {
        AdventInputSource::Sample(_) => info!("         Sample: {SAMPLE_ONE}"),
        AdventInputSource::Input => info!("         Result: {RESULT_ONE}"),
    }
}

fn first_and_last_digit(line: &str) -> Option<u32> {
    let digit_1 = nth_digit(line, Some(1))?;
    let digit_2 = nth_digit(line, None)?;

    Some(digit_1 * 10 + digit_2)
}

fn part_two(input: &ProblemInput) {
    let result: u32 = input
        .lines
        .iter()
        .map(|line| replace_number_words(line))
        .filter_map(|line| first_and_last_digit(&line))
        .sum();
    info!("Day {DAY:<2} - Part 2: {result}");
    match &input.source {
        AdventInputSource::Sample(_) => info!("         Sample: {SAMPLE_TWO}"),
        AdventInputSource::Input => info!("         Result: {RESULT_TWO}"),
    }
}

fn replace_number_words(line: &str) -> String {
    let mut output = String::new();
    let len = line.len();
    let last = len - 1;
    let mut u = 0;

    while u <= last {
        let chunk = line.chars().skip(u);
        let c = chunk.clone().take(1).collect::<String>();
        let c_3 = chunk.clone().take(3).collect::<String>();
        let c_4 = chunk.clone().take(4).collect::<String>();
        let c_5 = chunk.take(5).collect::<String>();

        if let Some(v) = match_threes(&c_3) {
            output.push(v);
            u += 2;
        } else if let Some(v) = match_fours(&c_4) {
            output.push(v);
            u += 3;
        } else if let Some(v) = match_fives(&c_5) {
            output.push(v);
            u += 4;
        } else {
            output.push_str(&c);
            u += 1;
        }
    }

    output
}

fn match_threes(chunk: &str) -> Option<char> {
    match chunk {
        "one" => Some('1'),
        "two" => Some('2'),
        "six" => Some('6'),
        _ => None,
    }
}

fn match_fours(chunk: &str) -> Option<char> {
    match chunk {
        "four" => Some('4'),
        "five" => Some('5'),
        "nine" => Some('9'),
        _ => None,
    }
}

fn match_fives(chunk: &str) -> Option<char> {
    match chunk {
        "three" => Some('3'),
        "seven" => Some('7'),
        "eight" => Some('8'),
        _ => None,
    }
}
