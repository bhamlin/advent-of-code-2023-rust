use itertools::Itertools;
use std::{collections::HashMap, fmt::Display};

#[derive(Debug, Default)]
pub struct ShiftRange {
    src: u64,
    dest: u64,
    len: u64,
}

impl Display for ShiftRange {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "[{}..={}] -> [{}..={}]",
            self.src,
            self.src + self.len,
            self.dest,
            self.dest + self.len
        )
    }
}

#[derive(Debug, Default)]
pub struct RangeMap {
    maps: HashMap<u64, ShiftRange>,
    chunks: Vec<u64>,
}

impl Display for RangeMap {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let mut message = String::from("RangeMap { ");

        message += self
            .maps
            .values()
            .map(ShiftRange::to_string)
            .collect_vec()
            .join("; ")
            .as_str();

        message += " }";
        write!(f, "{}", message)
    }
}

impl RangeMap {
    pub fn add_range(&mut self, dest: u64, src: u64, len: u64) {
        self.maps.insert(src, ShiftRange { src, dest, len });

        // Clear and update to prevent constant reallocation in struct.
        // Despite that being exactly what collect_vec is doing. ¯\_(ツ)_/¯
        self.chunks.clear();
        self.chunks
            .extend(self.maps.keys().sorted().cloned().collect_vec());
    }

    pub fn get(&self, index: u64) -> u64 {
        let mut offset: u64 = 0;
        let mut start = index;

        for src in self.chunks.iter() {
            if src > &index {
                break;
            } else {
                let rm = self.maps.get(src).unwrap();
                let dest = rm.dest;
                let len = rm.len;
                if index >= (*src + len) {
                    continue;
                } else {
                    offset = index - *src;
                    start = dest
                }
            }
        }

        start + offset
    }
}
