mod almanac;
mod tools;

use std::cmp;

use crate::almanac::{Almanac, Section};
use aoc_tools::{advent_init, error::Nothing, AdventInputSource, AdventProblemPart, ProblemInput};
use log::*;
use rayon::prelude::*;

const DAY: u8 = 5;
const SAMPLE_ONE: i128 = 35;
const SAMPLE_TWO: i128 = 46;
const RESULT_ONE: i128 = 218_513_636;
const RESULT_TWO: i128 = 81_956_384;

fn main() -> Nothing {
    let input = advent_init(DAY)?;

    match &input.part {
        AdventProblemPart::One => part_one(&input),
        AdventProblemPart::Two => part_two(&input),
    }

    Ok(())
}

fn part_one(input: &ProblemInput) {
    info!("Building almanac...");
    let almanac = Almanac::load_from_input_singles(input);
    info!(" - Seed list size: {}", almanac.seeds.len());

    info!("Finding min loc...");
    // let result = almanac
    //     .seeds
    //     .iter()
    //     .map(|&seed| almanac.lookup_seed(seed).location)
    //     .fold(almanac.get_a(Section::Location), cmp::min);

    let mut result = almanac.get_a(Section::Location);
    for seed in almanac.seeds.iter() {
        let loc = almanac.lookup_seed(*seed).location;
        result = cmp::min(result, loc);
    }

    info!("Day {DAY:<2} - Part 1: {result}");
    match &input.source {
        AdventInputSource::Sample(_) => info!("         Sample: {SAMPLE_ONE}"),
        AdventInputSource::Input => info!("         Result: {RESULT_ONE}"),
    }
}

fn part_two(input: &ProblemInput) {
    info!("Building almanac...");
    let almanac = Almanac::load_from_input_ranges(input);
    info!("Seed list size: {}", almanac.seeds.len());

    // info!("Finding min loc...");
    // let mut result = almanac.get_a(Section::Location);
    // for seed in almanac.seeds.iter() {
    //     let loc = almanac.lookup_seed(*seed).location;
    //     result = cmp::min(result, loc);
    // }

    info!("Finding min loc...");
    let chunk_size = 1000000;
    let output: Vec<u64> = almanac
        .seeds
        .par_chunks(chunk_size)
        .map(|chonk| {
            debug!("chunk: {chonk:?}");
            chonk
                .iter()
                .map(|&seed| almanac.lookup_seed(seed).location)
                .fold(almanac.get_a(Section::Location), cmp::min)
        })
        .collect();
    let result = output.iter().fold(output.first().unwrap(), cmp::min);

    info!("Day {DAY:<2} - Part 2: {result}");
    match &input.source {
        AdventInputSource::Sample(_) => info!("         Sample: {SAMPLE_TWO}"),
        AdventInputSource::Input => info!("         Result: {RESULT_TWO}"),
    }
}
