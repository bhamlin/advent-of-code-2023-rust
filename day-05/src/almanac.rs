use crate::tools::RangeMap;
use aoc_tools::ProblemInput;
use itertools::Itertools;
use log::trace;
use std::{collections::HashMap, fmt::Display, hash::Hash};

#[derive(Debug, Clone, Copy, Eq, Hash, Ord, PartialEq, PartialOrd)]
pub enum Section {
    Seed,
    Soil,
    Fertilizer,
    Water,
    Light,
    Temperature,
    Humidity,
    Location,
}

impl Display for Section {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let message = match self {
            Self::Seed => "Seed",
            Self::Soil => "Soil",
            Self::Fertilizer => "Fertilizer",
            Self::Water => "Water",
            Self::Light => "Light",
            Self::Temperature => "Temperature",
            Self::Humidity => "Humidity",
            Self::Location => "Location",
        };
        write!(f, "{message}")
    }
}

pub struct SeedRecord {
    pub seed: u64,
    pub soil: u64,
    pub fertilizer: u64,
    pub water: u64,
    pub light: u64,
    pub temperature: u64,
    pub humidity: u64,
    pub location: u64,
}

impl Display for SeedRecord {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "{} -> soil:{} fert:{} water:{} light:{} temp:{} humid:{} loc:{}",
            self.seed,
            self.soil,
            self.fertilizer,
            self.water,
            self.light,
            self.temperature,
            self.humidity,
            self.location
        )
    }
}

#[derive(Debug)]
pub struct Almanac {
    pub seeds: Vec<u64>,
    maps: HashMap<Section, RangeMap>,
}

impl Almanac {
    pub fn load_from_input_singles(input: &ProblemInput) -> Self {
        let seeds = if let Some((_, seed_string)) = input.lines.first().unwrap().split_once(": ") {
            seed_string
                .split_whitespace()
                .flat_map(str::parse)
                .collect()
        } else {
            Vec::default()
        };

        Almanac::load_vec(input, seeds)
    }

    pub fn load_from_input_ranges(input: &ProblemInput) -> Self {
        let seeds = if let Some((_, seed_string)) = input.lines.first().unwrap().split_once(": ") {
            seed_string
                .split_whitespace()
                .flat_map(str::parse::<u64>)
                .collect()
        } else {
            Vec::default()
        };

        let seeds = seeds
            .chunks(2)
            .flat_map(|pair| {
                let (i, n) = (pair[0], pair[1]);
                (i..i + n).collect_vec()
            })
            .collect_vec();

        Almanac::load_vec(input, seeds)
    }

    fn load_vec(input: &ProblemInput, seeds: Vec<u64>) -> Self {
        trace!("Seeds: {seeds:?}");
        let mut maps: HashMap<Section, RangeMap> = Default::default();

        let mut state = Section::Seed;
        for line in input.lines.iter().skip(2) {
            if !line.is_empty() {
                if line.starts_with("seed-to-soil") {
                    state = Section::Soil;
                    trace!("State: {state}");
                } else if line.starts_with("soil-to-fertilizer") {
                    state = Section::Fertilizer;
                    trace!("State: {state}");
                } else if line.starts_with("fertilizer-to-water") {
                    state = Section::Water;
                    trace!("State: {state}");
                } else if line.starts_with("water-to-light") {
                    state = Section::Light;
                    trace!("State: {state}");
                } else if line.starts_with("light-to-temperature") {
                    state = Section::Temperature;
                    trace!("State: {state}");
                } else if line.starts_with("temperature-to-humidity") {
                    state = Section::Humidity;
                    trace!("State: {state}");
                } else if line.starts_with("humidity-to-location") {
                    state = Section::Location;
                    trace!("State: {state}");
                } else {
                    // Inlining split_whitespace_and_parses
                    if let Some((dest, src, len)) =
                        line.split_whitespace().flat_map(str::parse).collect_tuple()
                    {
                        trace!("- Ingesting {} from: {:?}", state, (dest, src, len));
                        maps.entry(state).or_default().add_range(dest, src, len);
                    }
                }
            }
        }

        Almanac { seeds, maps }
    }

    pub fn get_a(&self, section: Section) -> u64 {
        let index = self.seeds.first().unwrap();
        self.lookup_by_map(section, *index)
    }

    pub fn lookup_by_map(&self, section: Section, index: u64) -> u64 {
        self.maps.get(&section).unwrap().get(index)
    }

    pub fn lookup_seed(&self, seed: u64) -> SeedRecord {
        let soil = self.lookup_by_map(Section::Soil, seed);
        let fertilizer = self.lookup_by_map(Section::Fertilizer, soil);
        let water = self.lookup_by_map(Section::Water, fertilizer);
        let light = self.lookup_by_map(Section::Light, water);
        let temperature = self.lookup_by_map(Section::Temperature, light);
        let humidity = self.lookup_by_map(Section::Humidity, temperature);
        let location = self.lookup_by_map(Section::Location, humidity);

        SeedRecord {
            seed,
            soil,
            fertilizer,
            water,
            light,
            temperature,
            humidity,
            location,
        }
    }
}

// fn split_whitespace_and_parse<T: std::str::FromStr>(input: &str) -> Vec<T> {
//     input.split_whitespace().flat_map(str::parse::<T>).collect()
// }
